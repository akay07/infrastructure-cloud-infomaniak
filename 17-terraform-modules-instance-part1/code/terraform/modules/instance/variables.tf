variable "instance_network_external_name" {
  type    = string
}

variable "instance_network_external_id" {
  type = string
}

variable "instance_name" {
  type = string
}

variable "instance_image_id" {
  type = string
  default = "cdf81c97-4873-473b-b0a3-f407ce837255"
}

variable "instance_flavor_name" {
  type    = string
  default = "a1-ram2-disk20-perf1"
}

variable "instance_security_groups" {
  type    = list(any)
  default = ["default"]
}

variable "instance_key_pair" {
  type = string
}

variable "metadatas" {
  type = map(string)
  default = {
    "environment" = "dev"
  }
}

variable "instance_network_internal" {
  type    = string
}
