%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

We have a problem ??

<br>

For one instance, as we see :

    * 1 data source : openstack_networking_subnet_ids_v2

    * 1 resource : openstack_networking_floatingip_v2

    * 1 resource : openstack_compute_instance_v2

    * 1 resource : openstack_compute_floatingip_associate_v2

    * x resources : volumes, ports...

<br>

We need to refactor it !!!

  * solution 1 resource to manage them all = module (like a role in ansible)



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Module concept :

    * like a role in ansible, like a big function

    * variable > module (resources) > output

    * a workspace : can be versionned (git dedicated repository)

    * can be share and mutualize between terraform projects

    * coulmd be used to structure your terraform code (no sub-directory)

    * a declarative mode


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

An example

```
module "openvpn" {
  source                      = "../modules/instances"
  instance_name               = "openvpn"
  instance_image_id           = "cdf81c97-4873-473b-b0a3-f407ce837255"
  instance_flavor_name        = "a1-ram2-disk20-perf1"
  instance_ssh_key            = var.ssh_public_key_default_user
  instance_key_pair           = openstack_compute_keypair_v2.ssh_public_key.name
  instance_security_groups    = ["ssh-from-all","openvpn","all_internal"]
  instance_network_name       = "internal_dev"
  public_floating_ip          = true
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

An example

```
instances
├── instance.tf
├── outputs.tf
├── variables.tf
└── volumes.tf
```


-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : variables

```
variable "network_name" {
  type = string
}
variable "network_subnet_cidr" {
  type = string
}
variable "network_subnet_ip_version" {
  type = number
  default = 4
}
```



-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : variables

```
variable "network_subnet_dhcp_enable" {
  type = string
  default = "true"
}
variable "network_subnet_dns" {
  type = list
  #default = ["1.1.1.1","8.8.8.8"]
  default = ["0.0.0.0"]
}
variable "router_id" {
  type = string
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : resource

```
resource "openstack_networking_network_v2" "network_name" {
  name = var.network_name
  admin_state_up = "true"
}
```

```
resource "openstack_networking_subnet_v2" "network_subnet" {
  name = var.network_name
  network_id = openstack_networking_network_v2.network_name.id
  cidr = var.network_subnet_cidr
  ip_version = var.network_subnet_ip_version
  enable_dhcp = var.network_subnet_dhcp_enable
  dns_nameservers = var.network_subnet_dns
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : resource

```
resource "openstack_networking_router_interface_v2" "network_router_interface" {
  router_id = var.router_id
  subnet_id = openstack_networking_subnet_v2.network_subnet.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : output

```
output "network_id" {
  description = "ID of the network"
  value       = openstack_networking_network_v2.network_name.id
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : version

```
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.52.1"
    }

  }
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Terraform - Module concept

<br>

Network module : usage

```
resource "openstack_networking_router_v2" "rt1" {
  name 			= "rt"
  admin_state_up 	= "true"
  external_network_id 	= var.network_external_id
}
module "network_dev" {
  source          = "../modules/networks"
  network_name		= var.network_internal_dev
  network_subnet_cidr	= var.network_subnet_cidr
  router_id		= openstack_networking_router_v2.rt1.id
}
```