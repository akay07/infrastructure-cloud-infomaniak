%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Purpose :

  * create role

  * create user

  * add ssh key & password (or nopassword)

  * add bashrc file

  * limit ssh users with allow file

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Refacto user variable

variable x 2 > module instance x 2 > variable > module

+ ansible x 2

Create a new role

```
ansible-galaxy init roles/users
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Add directory for user description

```
mkdir users
```

Then add xpestel.yml

```
password: password
no_pass: false
use_sudo: true
custom_bashrc: true
ssh_public_key: |
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINKkmKOi1M0P7zIG5SlCzCkC/DAmoO7CsGD2ANKtH5my oki@doki
```

And a bashrc file xpestel.bashrc

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Default variable (could autoconfigured)

```
users_default_account: xavki
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - main.yml

```
- name: Add admin users
  include_tasks: adminusers.yml
  with_fileglob:
  - "./users/*.yml"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - main.yml

```
- name: users list
  set_fact:
    user_list: [ "{{ users_default_account }}" ]

- name: users list
  set_fact:
    user_list:  "{{ [ item | basename | regex_replace('.yml$', '') ] + user_list }}"
  with_fileglob:
  - "./users/*.yml"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - main.yml

```
- name: change allowed users in sshd_config
  copy:
    content: "AllowUsers {{ user_list | join(' ') }}"
    dest: /etc/ssh/sshd_config.d/allowusers.conf
    owner: root
    group: root
    mode: 0640
    validate: sshd -t -f %s
  notify: restart_sshd
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - adminusers.yml

```
- name: load vars
  include_vars: 
    file: "{{ item }}"
    name: "user"

- name: define username
  set_fact:
    username:  "{{ item | basename | regex_replace('.yml$', '') }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - adminusers.yml

```
- name: "add user group {{ username }}"
  group:
    name: "{{ username }}"
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - adminusers.yml

```
- name: "add admin user {{ username }}"
  user:
    name: "{{ username }}"
    password: "{{ user.password | password_hash }}"
    update_password: on_create
    group: "{{ username }}"
    groups: sudo
    append: yes
    shell: /bin/bash
  changed_when: false
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - adminusers.yml

```
- name: add to sudoers file and validate
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: '^{{ username }} '
    line: "{{ username }} ALL=(ALL) {{ 'NOPASSWD:' if ( user.use_sudo_nopass|d(false) )  else '' }}ALL"
    validate: 'visudo -cf %s'
  environment:
    PATH: /usr/sbin:/usr/local/sbin:/sbin
  when: user.use_sudo == true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - adminusers.yml

```
- name: "add ssh keys {{ username }}"
  authorized_key:
    user: "{{ username }}"
    key: "{{ user.ssh_public_key }}"
    state: present
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Tasks - adminusers.yml

```
- name: copy custom bashrc if needed
  copy:
    src: "./users/{{ username }}.bashrc"
    dest: "/home/{{ username }}/.bashrc"
    owner: "{{ username }}"
    group: "{{ username }}"
    mode: 0750
  when: user.custom_bashrc == true
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - User Management

<br>

Handler

```
- name: restart_sshd
  systemd:
    name: sshd
    state: restarted
```

Add your role : be careful to your vpn server :)

```
    - users
```